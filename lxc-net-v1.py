import os
import sys
import subprocess
import re
import signal
from pathlib import Path

#Default variables used in the script

varrun="/var/run"
varlib="/var/lib"
USE_LXC_BRIDGE="true"
LXC_BRIDGE="lxcbr2"
LXC_BRIDGE_MAC="00:16:3e:00:00:00"
LXC_ADDR="10.0.3.1"
LXC_NETMASK="255.255.255.0"
LXC_NETWORK="10.0.3.0/24"
LXC_DHCP_RANGE="10.0.3.2,10.0.3.254"
LXC_DHCP_MAX="253"
LXC_DHCP_CONFILE=""
LXC_DHCP_PING="true"
LXC_DOMAIN=""
use_iptables_lock="-w"

def execute(cmd):
        execute_thread = subprocess.Popen(cmd.split(),stderr=subprocess.PIPE,stdout=subprocess.PIPE)
        output,error = execute_thread.communicate(input=cmd)
        if output:
                with open('/tmp/lxc-net.stdout','a') as Stdout:
                        Stdout.write('##############################################################\n')
                        Stdout.write(cmd+'\n')
                        Stdout.write(output+'\n')
                        Stdout.write('command returncode'+' '+str(execute_thread.returncode))
                        Stdout.write('\n##############################################################\n')
        if error:
                with open('/tmp/lxc-net.stderr','a') as StdErr:
                        StdErr.write('##############################################################\n')
                        StdErr.write(cmd+'\n')
                        StdErr.write(error+'\n')
                        StdErr.write('command returncode'+'  '+str(execute_thread.returncode))
                        StdErr.write('\n##############################################################\n')


def netmask2cidr(netmask):
        ipregex=re.compile('255.')
        hostid=re.sub(ipregex,"",netmask)
        diff=(len(netmask)-len(hostid))*2
        pattern1=re.compile('{0}.*'.format(hostid.split('.')[0]))
        net_mask=re.sub(pattern1,"",'0^^^128^192^224^240^248^252^254^')
        return  diff+(len(net_mask)/4)

def nicdown():
     execute('ip addr flush dev '+LXC_BRIDGE)
     print "Bridge Interface {0} is brought down".format(LXC_BRIDGE)

def nicup():
        mask=netmask2cidr(LXC_NETMASK)
        cidr_addr=LXC_ADDR+'/'+str(mask)
        print 'configuring {0} with subnet {1}.'.format(LXC_BRIDGE,cidr_addr)
        execute('ip addr add '+cidr_addr+' dev '+LXC_BRIDGE)
        execute('ip link set dev '+LXC_BRIDGE+' address '+LXC_BRIDGE_MAC)
        execute('ip link set dev '+LXC_BRIDGE+' up')
        print "Bridge Interface {0} is brought up".format(LXC_BRIDGE)

def start():
        print "---Bringing up the Network---"
        if USE_LXC_BRIDGE != "true":
                sys.exit(2)
        if os.path.isfile(os.path.join(varrun,'network_up')):
                print "lxc-net is already running"
                sys.exit(3)
        if os.path.isdir(os.path.join('/sys/class/net/',LXC_BRIDGE)):
                print "Bridge {0} is already running,can't start the lxc-net.Alternatively clean up the bridge or change the LXC_BRIDGE name.".format(LXC_BRIDGE)
                sys.exit()
        FAILED=1
        def cleanup(signum,frame):
                execute('set +e')
                if FAILED==1:
                        print "Failed to setup lxc-net."
                        stop(force=True)
                        sys.exit(4)
        signal.signal(signal.SIGINT,cleanup)
        signal.signal(signal.SIGINT,cleanup)
        signal.signal(signal.SIGHUP,cleanup)
	subprocess.check_call(['set -e'],shell=True)
        if not os.path.isdir(os.path.join('/sys/class/net/',LXC_BRIDGE)):
                execute('ip link add dev '+LXC_BRIDGE+' type bridge')
        with open(os.path.abspath('/proc/sys/net/ipv4/ip_forward'),'w') as ip_forward:
                ip_forward.write('1')
        nicup()
        iptable_rule=['-I INPUT -i {0} -p udp --dport 67 -j ACCEPT','-I INPUT -i {0} -p tcp --dport 67 -j ACCEPT','-I INPUT -i {0} -p udp --dport 53 -j ACCEPT','-I INPUT -i {0} -p tcp --dport 53 -j ACCEPT','-I FORWARD -i {0} -j ACCEPT','-I FORWARD -o {0} -j ACCEPT', '-t nat -A POSTROUTING -s {1} ! -d {1} -j MASQUERADE','-t mangle -A POSTROUTING -o {0} -p udp -m udp --dport 68 -j CHECKSUM --checksum-fill']
        iptable_rule_grained=list(map(lambda rule_: rule_.format(LXC_BRIDGE,LXC_NETWORK),iptable_rule))
        for rule in iptable_rule_grained:
                execute('iptables '+use_iptables_lock+' '+rule)

        LXC_DOMAIN_ARG=""
        if LXC_DOMAIN == "":
                LXC_DOMAIN_ARG="-s {0} -S /{0}/".format('lxc.local')
        else:
                LXC_DOMAIN_ARG="-s {0} -S /{0}/".format(LXC_DOMAIN)
        LXC_DHCP_CONFILE_ARG=""
        if LXC_DHCP_CONFILE_ARG == "":
                LXC_DHCP_CONFILE_ARG="--conf-file={0}".format(LXC_DHCP_CONFILE)
        DEVNULL=open(os.devnull,'w')
        for DNSMASQ_USER in 'lxc-dnsmasq','dnsmasq','nobody':
                try:
                       subprocess.check_call(['getent','passwd',DNSMASQ_USER],stdout=DEVNULL,stderr=DEVNULL)
                       break
                except subprocess.CalledProcessError as err:
                       pass
        LXC_DHCP_PING_ARG=""
        if LXC_DHCP_PING == "false":
                LXC_DHCP_PING_ARG="--no-ping"
        if subprocess.check_call(['dnsmasq',LXC_DHCP_CONFILE_ARG,LXC_DOMAIN_ARG,LXC_DHCP_PING_ARG,'-u',DNSMASQ_USER,'--strict-order','--bind-interfaces','--pid-file={0}/dnsmasq.pid'.format(varrun),'--listen-address',LXC_ADDR,'--dhcp-range',LXC_DHCP_RANGE,'--dhcp-lease-max={0}'.format(LXC_DHCP_MAX),'--dhcp-no-override','--except-interface=lo','--interface={0}'.format(LXC_BRIDGE),'--dhcp-leasefile={0}/misc/dnsmasq.{1}.leases'.format(varlib,LXC_BRIDGE),'--dhcp-authoritative'],stdout=DEVNULL,stderr=DEVNULL) != 0:
                stop(force=True)
                sys.exit(5)
        Path(os.path.join(varrun,'network_up')).touch()
        print "dnsmasq servic is live ,serving DNS cache and DHCP"
        FAILED=0

def stop(force=None):
        if USE_LXC_BRIDGE != "true":
                sys.exit(2)
        if not os.path.isfile(os.path.join(varrun,'network_up')):
                if not force:
                        print "lxc-net isn't running"
                        sys.exit(6)
        print "---Bringing down the Network---"
	if os.path.isdir(os.path.join('/sys/class/net/',LXC_BRIDGE)) :
                nicdown()
                iptable_rule=['-D INPUT -i {0} -p udp --dport 67 -j ACCEPT','-D INPUT -i {0} -p tcp --dport 67 -j ACCEPT','-D INPUT -i {0} -p udp --dport 53 -j ACCEPT','-D INPUT -i {0} -p tcp --dport 53 -j ACCEPT','-D FORWARD -i {0} -j ACCEPT','-D FORWARD -o {0} -j ACCEPT', '-t nat -D POSTROUTING -s {1} ! -d {1} -j MASQUERADE','-t mangle -D POSTROUTING -o {0} -p udp -m udp --dport 68 -j CHECKSUM --checksum-fill']
                iptable_rule_grained=list(map(lambda rule_: rule_.format(LXC_BRIDGE,LXC_NETWORK),iptable_rule))
                for rule in iptable_rule_grained:
                        execute('iptables '+use_iptables_lock+' '+rule)
                with open(os.path.join(varrun,'dnsmasq.pid'),'r') as pid:
                        pidn=int(pid.read().rstrip('\n'))
                        os.kill(pidn,9)
                        print 'dnsmasq has been terminated'
                os.remove(os.path.join(varrun,'dnsmasq.pid'))
                if not os.listdir(os.path.join('/sys/class/net/',LXC_BRIDGE,'brif')) :
                        execute('ip link delete '+LXC_BRIDGE)
        os.remove(os.path.join(varrun,'network_up'))


class Case:
        def __init__(self,argv):
                self.argv=argv
        def start(self):
                return start()
        def stop(self):
                return stop()
        def restart(self):
                self.stop()
                self.start()
        def default(self):
                print "Usage: $0 {start|stop|restart}"
                sys.exit(2)
        def case(self):
                func={'start':self.start,'stop':self.stop,'restart':self.restart}.get(self.argv,self.default)
                return func()


try:
        DEVNULL=open(os.devnull,'w')
        if subprocess.check_call(['iptables','-w','-L','-n'],stdout=DEVNULL,stderr=DEVNULL) != 0:
                use_iptables_lock=""
except subprocess.CalledProcessError as err:
        print err
        sys.exit(1)


if __name__ == "__main__":
        service=Case(sys.argv[1])
        service.case()


