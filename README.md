#Author : g.sabarinath
#contact: g.sabarinath91@gmail.com

Description:
   A python script to control the bridge network for LXC container.
  
How lt's work?
   * Script will have a few global  variables that define bridge interface,network,DHCP range and etc.
   * dnsmasq is used which is a lightweight DNS, TFTP, PXE, router advertisement and DHCP server. It is intended to provide coupled DNS and DHCP service to a LAN.
   * As a whole the brige adapter is used as virtual switch to server traffic between containers along with dnsmasq.
   * Tweak the /etx/lxc/default.conf file to tell lxc network link.
   * A newly launched container will get assigned IP address by dnsmasq DHCP.

How to setup?
   git clone https://gsabarinath91@bitbucket.org/gsabarinath91/lxc-net.git
   cd lxc-net
   pip install -r requiremets.txt
  
How to run?
    python lxc-net-v1.py start
	python lxc-net-v1.py stop
	python lxc-net-v1.py restart
	
	
 

